Vztažná zájmena
===============


|     |            |           |            |           |
| --- | ---        | ---       | ---        | ---       |
| 1.p | der        | die       | das        | die       |
| 2.p | **dessen** | **deren** | **dessen** | **deren** |
| 3.p | dem        | der       | dem        | **denen** |
| 4.p | den        | die       | das        | die       |

## Příklady
1. Was kostet die Zeitung, __die__ du liest?
2. Das ist der Kollege, von __dem__ du sprichst?
3. Ruf die Frau an, __deren__ Auto hier steht.
4. Es kommen alle Leute, __deren__ Kinder hier spielen.
5. Bringe das Geld, ohne __das__ wir es nicht kaufen können.
---
1. Was kostet das Buch, __das__ du liest?
2. Das ist die Kollegin, von __der__ du sprichst.
3. Ruf den Mann an, __dessen__ Auto hier steht.
4. Es kommen alle Freuden, __deren__ Kinder hier spielen.
5. Bringe das Geld, ohne __das__ wir es nicht kaufen.
---
1. Was kostet das Bier, __das__ du kaufst.
2. Das ist der Kollege, von __dem__ du sprichst.
3. Ruf die Frau an, __deren__ Auto hier steht.
4. Es kommt der Student, __dessen__ Bücher hier sind.
5. Bringe die Bücher, ohne __die__ wir es nicht machen können.
---
1. Dr zug, mit __dem__ wir fahren.
2. Das ist mein Freund, __dessen__ Mutter Mathematiklehrerin ist.
3. Ist das die Schule, __die__ er besucht.
4. Das sind die Häuser, __den__ du dir ansehen möchtest.
5. Der Tourist, von __dem__ wir es erfahren haben.

## Nevyplňené příklady
1. Was kostet das Buch, ____ du liest?
2. Das ist die Kollegin, von ____ du sprichst.
3. Ruf den Mann an, ____ Auto hier steht.
4. Es kommen alle Freuden, ____ Kinder hier spielen.
5. Bringe das Geld, ohne ____ wir es nicht kaufen.

1. Was kostet das Bier, ____ du kaufst.
2. Das ist der Kollege, von ____ du sprichst.
3. Ruf die Frau an, ____ Auto hier steht.
4. Es kommt der Student, ____ Bücher hier sind.
5. Bringe die Bücher, ohne ____ wir es nicht machen können.
