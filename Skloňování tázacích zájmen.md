Skloňování tázacích zájmen
==========================

| pád |        |      |
|-----|--------|------|
| 1.  | wer    | kdo  |
| 2.  | wessen | čí   |
| 3.  | wem    | komu |
| 4.  | wen    | koho |


| pád |     |    |
|-----|-----|----|
| 1.  | was | co |
| 2.  |     |    |
| 3.  |     |    |
| 4.  | was | co | 

