Požadavky CVUT
==============

* préteritum + perfektum
* ~~rozkazovací způsob~~
* ~~stupňování~~
* ~~časové spojky: bis, wenn, als, dass, damit~~
* příčestí minulé/přítomné
* zájmenné příslovce (woraus, dabei
* předložky (aus, darauf, worin)
* ~~vztažné zájmena~~
* ~~infinitiv s/bez zu~~
* ~~koncovky~~
* ~~spojky~~
* ~~dieselbe, dasselbe, dieselben~~
* ~~zvratná slovesa~~
* ~~směrová příslovce~~
* ~~řadové číslovky~~