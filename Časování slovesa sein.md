Časování slovesa sein
=====================
|           |      |      |
| ---       | ---  | ---  |
| ich       | bin  | jsem |
| du        | bist | jsi  |
| er/sie/es | ist  | je   |
| wir       | sind | jsme |
| ihr       | seid | jste |
| sie/Sie   | sind | jste |