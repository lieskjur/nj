Závislý infinitiv
=================

## na podstatném a přídavném jměnu

|                                                              |                                   |
| ---                                                          | ---                               |
| Sie hat die **Möglichkeit**, nach Deutschland **zu fahren**. | Má možnost jet do Německa.        |
| Es ist **notwendig**, es ihm **zu sagen**.                   | Je nutné mu to říct.              |
| Es war unangenehm, so früh **aufzustehen**.                  | Bylo nepříjemné vstávat tak brzy. |
| Hast du Zeit, es dir **anzuhören**?                          | Máš čas si to poslechnout?        |
| Hast du Lust, mit uns ins Kino **zu gehen**?                 | Máš chuť jít s námi do kina?      |

## na významovém slovesu

* Částice "zu" se přidává k infinitivu, pokud je závislý na jiném významovém slovesu.
	* Píše se zvlášť, pouze u sloves s odlučitelnou předponou se vkládá mezi předponu a základní sloveso
* Prostý infinitiv bez částice "zu" následuje po
	* způsobových slovesech (dürfen, können, mögen, müssen, wollen, sollen, wissen)
	* slovesech vyjadřující pohyb (fahren, laufen, gehen, kommen)
	* slovesech vnímání (sehen, hören)
	* a některých dalších (bleiben, lassen, schicken)
* Je-li závislý infinitiv rozvitější, stojí u něho částice "zu".

|                                                |                             |
| ---                                            | ---                         |
| Er **beggint** Englisch **zu** lernen.         | Začíná se učit anglicky.    |
| **Vergiss** nicht das Fenster auf**zu**machen! | Nezapomeň otevřít okno!     |
| Es **lohnt** sich nicht mehr hin**zu**fahren.  | Už se nevyplatí tam jezdit. |
| Sie **brauchen** es nicht **zu** sagen.        | Nemusíte to říkat.          |
| Wir wollen danach fragen.                      | Chceme se na to zeptat.     |
| Ich sehe ihn kommen.                           | Vidím ho přicházet.         |
| Er lässt sich alles gefallen.                  | Nechá si všechno líbit.     |

## na slovesech "haben" a "sein"

|                                                  |                                    |
| ---                                              | ---                                |
| Die Sache **ist** nicht **zu** bekommen          | Ta věc není k dostání              |
| Das Problem **ist** noch **zu** lösen            | Ten problém je třeba ještě vyřešit |
| Wir **haben** viel **zu** tun                    | Máme mnoho práce                   |
| Er **hat** dieses Problem **zu** lösen           | Musí tento problém vyřešit         |
| Wir **haben** noch zwei Übungen **zu** schrieben | Musíme napsat ještě dvě cvičení    |
| Der Brief **ist** noch **zu** schreiben          | Ten dopis je třeba ještě napsat.   |
| Du **hast** den Brief **zu** schreiben           | Ty musíš ten dopis napsat          | 

## Příklady z testů
---
1. Man muss schnell **laufen**. 
2. Es ist gut es **zu lernen**. 
3. Du hörst nicht **Hans spielen**. 
4. Wir gehen jetzt **Schlittschuhfahren**. 
5. Ich versuche **zu kommen**. 
---
1. Man will deutsch **lernen**.
2. Es ist praktisch es **zu wissen**.
3. Du siehst nicht **Hans spielen**.
4. Wir fahren jetzt **einkaufen**.
5. Ich versuche es **zu kommen**.
---
1. Ich hatte grosse Probleme **Englisch *zu* verstehen**. (Englisch verstehen)
2. Ich kann mich nicht **auf das Studium konzentrieren**. (auf das Studium konzentrieren)
3. Er empfiehlt dir **mit*zu*fahren**. (mitfahren)
4. Wir hatten vor **baden *zu* gehen**. (baden gehen)
5. Ich höre **Eva singen**. (Eva singen)
---
1. Kommen Sie, wir müssen ____ gehen.
2. Ich vergesse nicht ihn __zu__ anrufen.
3. Hören Sie das Mädchen ____ singen?
4. Es ist gut, sich öfter __zu__ treffen.
5. Es muss so ____ sein.
6. Wie lange können Sie ____ bleiben?
---
1. Wir gehen ____ schlafen.
2. Ich vergesse nicht ihm __zu__ schreiben.
3. Sehen Sie das Mädchen ____ kommen?
4. Es ist notwendig, öfter __zu__ üben.
5. Es ist nichts __zu__ sehen.
6. Wie lange können Sie ____ bleiben?