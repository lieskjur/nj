ab/holten | vyzvednout
Einzelheiten | podrobnosti
leidenschaftlicher | vášnivý
bummeln | procházet se
leidenschaftlicher | vášnivý
Fachwerkhäuser | hrázděné domy
ausrüstet | vybavený
entschlossen | rozhodnutý
verbringen | strávit
belohnung | odměna
erinnern | připomenout

Strausee | přehrada
Urlaub | dovolená
zelten | stanovat
ausüben | provozovat
rudern | veslování
paddeln | veslování
segeln | plachtění
Erlebnis | dobrodružství
vertragen | tolerovat
Dampferfahrt | jízda parníkem
besonders | zvláště
nämlich | a to

fleißig | pilný
Haltstelle | zastávka
Panne | porucha
Verspätung | zpoždění
schaffen | utvořit, udělat
wirklichschaftliche | hospodářská
Lage | situace
lohnen | oplatit se

begleiten | doprovodit
aufenhalt | pobyt


in zwei Wochen
Gratzener Bergland
tschundern / wandern

weh - bolavý 
behandlung | léčba
verschiedene | rozličný
umgebung | okolí
weh/tun | tat weh | hat wehgetan | bolet

Ich studiere seit fünf Jahren an der Tschechischen Technischen Universität, die letzten zwei Jahre Mechatronik.