Skloňování přidavných jmen v přívlastku
=

## Bez členu
| pád | mužský rod        | ženský rod      | střední rod       | množné číslo                         |
| --- | ----------        | -----------     | -----------       | ------------                         |
| 1.  | nett**er** Mann   | nett**e** Frau  | nett**es** Kind   | nett**e**   Männer, Frauen, Kinder   |
| 2.  | nett***en*** Mann | nett**er** Frau | nett***en*** Kind | nett**er**  Männer, Frauen, Kinder   |
| 3.  | nett**em** Mann   | nett**er** Frau | nett**em** Kind   | nett**en**  Männern, Frauen, Kindern |
| 4.  | nett**en** Mann   | nett**e** Frau  | nett**es** Kind   | nett**e**   Männer, Frauen, Kinder   |

Takto skloňujeme adjektiva po číslovkách typu **einig**, **viel**, **wenig**, **ander**, **ein paar**, atd.

## Po určitém
| pád | mužský rod               | ženský rod           | střední rod              | plurál
| --- | ----------               | ----------           | -----------              | ------
| 1.  | der nett***e*** Mann     | die nett***e*** Frau | das nett***e*** Kind     | die nett**en** Leute
| 2.  | des nett**en** Mann(e)s  | der nett**en** Frau  | des nett**en** Kind(e)s  | der nett**en** Leute
| 3.  | dem nett**en** Mann      | der nett**en** Frau  | dem nett**en** Kind      | den nett**en** Laute
| 4.  | den nett**en** Mann      | die nett***e*** Frau | das nett***e*** Kind     | die nett**en** Laute

Místo členu neurčitého můžeme také použít zájmena **dieser**, **jener**, **jeder**, **welcher**, **mancher**.

V množném čísle můžeme navíč místo členu užít i **alle**, **beide**, **keine**.

## Po neurčitém
| pád | mužský rod            | ženský rod            | střední rod           |
| --- | ----------            | ----------            | -----------           |
| 1.  | *ein* nett**er** Mann | eine nett**e** Frau   | *ein* nett**es** Kind |
| 2.  | eines nett**en** Mann | einer nett**en** Frau | eines nett**en** Kind |
| 3.  | einem nett**en** Mann | einer nett**en** Frau | einem nett**en** Kind |
| 4.  | einen nett**en** Mann | eine nett**e** Frau   | *ein* nett**es** Kind |

Místo členu neurčitého můžeme použít také přivlastňovací zájmena **mein**, **dein**, **unser**, ...
či záporné zájmeno **kein**.

---

Příčestí minulé v přísudku se skloňuje stejně jako přídavná jména v přísudku

## Příklady
1. all**e** klein**en** Kinder
2. wenig**e** neu**e** Schulen
3. unser**e** alt**en** Kinder
4. für dein**en** lieb**en** Onkel
5. mit kein**em** jung**en** Freunden
---
1. kein**e** klein**e** Tasche
2. drei neu**e** Schulen
3. unser alt**es** Buch
4. mit dein**em** lieb**en** Opa
5. ohne kein**e** jung**e** Freundin
---
1. Mein** gut**er** Bekannter kommt mit sein**er** jung**en** Freundin zu Besuch.
2. Viele neu**e** Studenten wohnen in dies**em** alt**en** Studentenheim.
3. Er isst gern warm**e** Speisen un und trinkt dazu gut**es** Bier.
4. Dies**er** jung**e** Lehrer ist ein sehr streng**er** Lehrer.
5. Welch**es** neu**e** Kleid ziehst ud an? Dies**es** schwarz**e** oder dies**es** rot**e**?
---
1. Ihr**e** jünger**e** Cousine lebt in ein**em** klein**en** Ort.
2. Einige neu**e** Studenten wohnen in dies**em** alter**en** Studentenheim.
3. Ich esse gern gebraten**en** Fisch mit gut**em** Kartoffelsalat.
4. Sie trägt gern ein**e** schwarz**e** Hose mit ein**er** weiß**en** Bluse.
5. Ein** schnell**es** Auto fährt durch dies**e** breit**e** Straße.
---
1. Er sprict über sein**en** gut**en** Freund und dies**e** bekannt**e** Universität.
2. All**e** klug**en** Studentinnen legten schwer**e** Prüfungen ab.
3. Er trinkt nich gern süß**en** Tee und kalt**e** Limonade
---
