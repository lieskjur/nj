Předložky
=========

## Předložky s 3. pádem
* aus - z
* außer - kromě, mimo
* bei - u, při
* mit - s
* nach - po, podle, do (u geograf. jmen)
* seit - od
* von - z, od, o
* zu - k

## Předložky s 4. pádem
* durch - skrz
* für - pro, za
* gegen - proti
* ohne - bez
* um - okolo, v, o

## Předložky s 3. a 4. pádem
* an - u, k, na (svislé ploše)
* auf - na (vodorovné ploše)
* in - v, do, za (časově)
* über - nad, přes, o (4.p)
* unter - pod
* vor - před
* hinter - za (místně)
* neben - vedle
* zwischen - mezi

- Wo? - Kde? → 3. pád
- Wohin? - Kam? → 4.pád

## Příklady

### 3. pádem
Pocházím ze Slovenska - Ich komme aus der Slowakei

### 3. a 4. pádem
* Leží to na stole - Es liegt auf dem Tisch (3.p)
* Polož to na stůl - Leg es auf den Tisch (4.p)