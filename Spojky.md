Spojky
======

## Souřadící spojky

### Neměnící slovosled
* und - a
* aber - ale
* oder - nebo
* sonder - nýbrž
* denn - neboť

### Měnící slovosled (nepřímý)
(Es ist kalt, trotzdem machen wir einen Ausflug)
* deshalb/darum - proto
* trotzdem - přesto
* sonst - jinak
* außerdem - kromě toho

## Podřadící spojky (⇒vedlejší věty ⇒ přísudek na konci věty)

* dass - že
* weil - protože
* obwohl - ačkoliv
* ob - zda
* bis - dokud ne-...
* wenn - když (opakovaný děj ve všech časech)
* als - když (neopakovaný děj v minulosti)
* während - během
* sobald - jakmile
* damit - ať

---

Pokud je věta první v souvětí, přehazujeme v hlavní větě podmět s přísudkem