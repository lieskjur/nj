Způsobová slovesa
=================

| česky inf.      | ich  | du     | er   | wir    | ihr   | sie    |
| ----------      | ---  | --     | --   | ---    | ---   | ---    |
| smět 		      | darf | darfst | darf | dürfen | dürft | dürfen |
| umět/moci       | kann | kannst | kann | können | könnt | können |
| chtít (mít rád) | mag  | magst  | mag  | mögen  | mögt  | mögen  |
| muset 		  | muss | musst  | muss | müssen | müsst | müssen |
| chtít (přát si) | will | willst | will | wollen | wollt | wollen |
| mít povinnost   | soll | sollst | soll | sollen | sollt | sollen |
| vědět 		  | weiß | weißt  | weiß | wissen | wisst | wissen |

## Minulý čas

| česky inf.      | německy inf. | präteritum | perfektum    |
| ----------      | ------------ | ---------- | ---------    |
| smět 		      | dürfen       | durfte     | habe gedurft |
| umět (moci)     | können       | konnte     | habe gekonnt |
| chtít (mít rád) | mögen        | mochte     | habe gemocht |
| muset 		  | müssen       | musste     | habe gemusst |
| chtít (přát si) | wollen       | wollte     | habe gewollt |
| mít povinnost   | sollen       | sollte     | habe gesollt |
| vědět 		  | wissen       | wusste     | habe gewusst |

## "bych"

| česky inf.      | ich     | du        | er      | wir      | ihr     | sie      |
| ----------      | ---     | --        | --      | ---      | ---     | ---      |
| smět 		      |         |           |         |          |         |          |
| umět/moci       |         |           |         |          |         |          | 
| chtít (mít rád) | möchte  | möchtest  | möchte  | möchten  | möchtet | möchten  |
| muset 		  |         |           |         |          |         |          |
| chtít (přát si) |         |           |         |          |         |          |
| mít povinnost   |         |           |         |          |         |          |
| vědět 		  |         |           |         |          |         |          |