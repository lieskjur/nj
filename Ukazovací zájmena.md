Ukazovací zájmena
=================

| mužský rod  | ženský rod  | střední rod |          |
| ----------  | ----------  | ----------- | -------- |
| dieser      | diese       | dieses      | tento    |
| ein solcher | eine solche | ein solches | takový   |
| derselbe    | dieselbe    | dasselbe    | ten samý |

## Dieser
skoloňuje se jako určitý člen

## Ein solcher
skloňuje se jako přídavné jméno po neurčitém členu

## Derselbe
skloňuje se jako uřčitý člen + přídavné jméno

| pád | mužský rod | ženský rod | střední rod | množné číslo |
| --- | ---------- | ---------- | ----------- | ------------ |
| 1.  | derselbe   | dieselbe   | dasselbe    | dieselben    |
| 2.  | desselben  | derselben  | desselben   | derselben    |
| 3.  | demselben  | derselben  | demselben   | denselben    |
| 4.  | denselben  | dieselbe   | dasselbe    | dieselben    |

## Příklady

* Dieser Mantel - Tento Kabát
* Den Mantel - Ten kabát
* Ein solche Hut - Takový klobouk
* Solche Hüte - Takové klobouky
---
1. Wir besuchten dieselbe Stadt.
2. Er fuhr mit derselben Bussen.
3. Die Geschenke waren für denselben Mann.
