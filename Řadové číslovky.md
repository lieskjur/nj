Řadové číslovky
===============

- 1. der, die, das *erste*
- 2. der, die, das zwei*te*
- 3. der, die, das *dritte*
- 4. der vier*te*
- 5. der fünf*te*
- 6. der sechs*te*
- 7. der sieb(en)*te*
- 8. der acht*e*
- 9. der neun*te*
- 10. der zehn*te*
- 11. der elf*te*
- 12. der zwolf*te*
- 13. der dreizehn*te*
- 19. der neunzehn*te*
- 20. der zwanzig*ste*