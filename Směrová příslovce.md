Směrová příslovce
================

* dort - tam (kde?)
* hin - tam (kam?)
* her - sem (kam?)
* draußen - venku
* hinüber - na druhou stranu
* oben - nahoře
* unten - dole
* herein - dovnitř (kam?)

---

nach unten/oben - dolů/nahoru

## Příklady

1. Geh bitte **nach oben**! (nahoru)
2. Wir müssen **hinüber** gehen! (na druhou stranu)
3. Wer ist **unten**? (dole)
4. Kommen Sie **herein**! (dovnitř)
---
1. Er wohnt **oben**. (nahoře)
2. Kommst du **nach unten**? (dolů)
3. **Draußen** regnet es. (Venku)
4. Wir fahren **(dort)hin**. (tam)
