Příčestí minulé a přítomné
==========================

## Příčestí minulé v přívlastku
* das gekaufte Buch
* ein gekauftes Buch
* kein gekauftes Buch
* dieses gekaufte Buch

### Příčestí minulé 
1. die geschriebene Aufgabe
2. das korrigierte Fehler
3. das getrunkene Bier
4. das gesehene Haus
5. das gekochte Essen
---
1. das gekochte Mittagessen
2. der übersetzte Text
3. der verbrachte Urlaub
4. die angefangene Arbeit

## Příklady
- die vergessenen Sachen
- der geschriebene Brief
- das verkaufte Auto 
- Alle empfohlenen Ausflüge
- keine entschiedenen Fragen
---
- Sie läuft schnell, damit se die kommenden Gäste begrüßen kann.
- Wir waren mit der empfohlenen Reise zufrieden.
- Das bestellte Mittagessen schmeckt gut.
- Im Theater waren hundert sitzende Menschen.
- Beim Arzt is jetzt ein wartender Patient.
