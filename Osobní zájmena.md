Osobní zájmena
==============

## Jednotné číslo
| pád	| 1.os.	| 2.os.	| 3.os.m.	| 3.os.ž.	| 3.os.s	|                        |
|-------|-------|-------|-----------|-----------|-----------| ---                    |
| 1.	| ich	| du 	| er		| sie		| es		|                        |
| 2.	| mein	| dein	| sein		| ihr		| sein		| přivlastňovací zájmena |
| 3.	| mir	| dir	| ihm		| ihr		| ihm		|                        |
| 4.	| mich	| dich	| ihn		| sie		| es		|                        |

## Množné číslo
| pád	| 1.os.	| 2.os.	| 3.os.	|
|-------|-------|-------|-------|
| 1.	| wir	| ihr 	| sie	|
| 2.	| unser	| euer	| ihr	|
| 3.	| uns	| euch	| ihnen	|
| 4.	| uns	| euch	| sie	|