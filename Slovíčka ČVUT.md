Slovíčka ČVUT
=============

## Auto
|                                                 |                                                      |
| ---                                             | ---                                                  |
| s Dach                                          | střecha                                              |
| e Windschutzsheibe <br>- Schutz <br>- Scheibe   | čelní sklo <br>- ochrana <br>- sklo                  |
| e Motorhaube <br>- Haube                        | kapota <br>- kapuce                                  |
| r Scheinwerfer <br>- Schein <br>- werfer        | světlomet <br>- lesk <br>- vrhač                     |
| s KZ-Kennzeichen <br>- kenn <br>- zeichen       | RZ (Registrační značka) <br>- poznávací <br>- značka |
| e Wagentür                                      | dveře (auta)                                         |
| s Rad                                           | kolo                                                 |
| s Rückfenster                                   | zadní okno                                           |
| s Lenkrad <br>- lenk                            | volant <br>- řídící                                  |
| e Handbremse                                    | ruční brzda                                          |

## Computer
|              |            |
|--------------|------------|
| r Bildschirm | obrazovka  |
| e Tastatur   | klávesnice |
| e Festplatte | pevný disk |

## Geometrische Formen
|                       |             |
|-----------------------|-------------|
| r Kreis, es, e        | kruh        |
| s Dreieck, (e)s, e    | trojúhelník |
| s Viereck, (e)s, e    | čtyřúhelník |
| r Kubus, -, Kuben     | krychle     |
| e Kugel, -, n         | koule       |
| r Kegel, s, -         | kužel       |
| e Pyramide, -, n      | jehlan      |
| r Quader, s, -        | kvádr       |

## Zeichenzubehör
|                   |          |
|-------------------|----------|
| s Lineal          | pravítko |
| r Zirkel          | kružítko |

## Linien
|                       |             |
|-----------------------|-------------|
| e Gerade, -, n        | přímka      |
| e Halbgerade, -, n    | polopřímka  |
| e Strecke, -, n       | úsečka      |

## Matematische Begriffe
|   |          |
|---|----------|
| + | plus     |
| - | minus    |
| = | gleich   |
| * | mal      |
| / | zu/durch |

- 1/2 einhalb (eine Halbe)
- 1/3 ein Drittel
- 1/4 ein Viertel

## Stoffe, Werkstoffe und Fertigungsverfahren
|                     |        |
|---------------------|--------|
| s Holz              | dřevo  |
| s Eisen, s, -       | železo |
| s Stahl, (e)s, ä- e | ocel   |
| e Legierung, -, en  | litina |

## ČVUT
| Tsechische Technishe Universität              | České Vysoké Učení Technické           |
| --------------------------------              | ----------------------------           |
| Fakultät fur Maschienbau                      | Fakulta strojní                        |
| Fakultät fur Elektrotechnik                   | Fakulta elektrotechnická               |
| Fakultät fur Architektur                      | Fakulta architektury                   |
| Fakultät fur Bauwesen                         | Fakulta stavební                       |
| Fakultät fur technische Physik und Kernphysik | Fakulta jaderná a fyzikálně inženýrská |
| Fakultät fur Transportwesen                   | Fakulta dopravní                       |
| Fakultät fur biomedizinische Technik          | Fakulta biomedicínského inženýrství    |

## Werkzeug
|                         |                |
|-------------------------|----------------|
| e Bohrmaschine, -, n    | vrtačka        |
| r Meißel, s, -          | dláto          |
| r Schraubenzieher, s, - | šroubovák      |
| r Schraubendreher, s, - | šroubovák      |
| e Feile, -, n           | pilník         |
| r Hammer, s, ä          | kladivo        |
| e Säge, -, n            | pila           |
| r Maulschlüssel, s, -   | vidlicový klíč |
| r Imbusschlüssel, s, -  | imbus          | 