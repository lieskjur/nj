Slovosled
=========

## Oznamovací věta
* přímý : podmět - sloveso - ostatní větné členy
* nepřímý : zdůrazněný větný člen - sloveso - podmět - ostatní větné členy

## Tázací věta
* doplňovací otázka: tázací zájmeno - sloveso - podmět - ostatní větné členy
* zjišťovací otázka: sloveso - podmět - ostatní větné členy (odpověď je ano/ne)