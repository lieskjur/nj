Vazby sloves
============

## Předložkové vazby
|                              |                         |
| ---                          | ---                     |
| antworten auf 4.p.           | odpovídat na            |
| arbeiten an 3.p.             | pracovat na             |
| aufpassen auf 4.p.           | dávat pozor na          |
| bestehen in 3.p.             | spočívat v              |
| bitten um                    | prosit o                |
| danken für                   | děkovat za              |
| denken an 4.p.               | myslet na               |
| sich entschuldigen bei       | omluvit se komu         |
| sich entschuldigen für       | omluvit se za           |
| erzählen von nebo über 4.p.  | vyprávět o              |
| fragen 4.p. nach             | ptát se koho na         |
| sich freuen auf 4.p.         | těšit se na             |
| sich freuen über 4.p         | těšit se z              |
| gehen um                     | jít o                   |
| helfen bei                   | pomoci s, při           |
| hoffen auf 4.p               | doufat v                |
| hören von nebo über 4.p.     | slyšet o                |
| sich interessieren für       | zajímat se o            |
| klagen über 4.p.             | stěžovat si na          |
| nachdenken über 4.p.         | přemýšlet o             |
| lesen von nebo über 4.p.     | číst o                  |
| schreiben von nebo über 4.p. | psát o                  |
| spären für nebo an 3.p.      | (u)šetřit na co, na čem |
| sprechen von nebo über       | psát o                  |
| teilnehmen an 3.p.           | (z)účastnit se čeho     |
| sich unterhalten über 4.p.   | bavit se o              |
| warten auf 4.p.              | čekat na                |

## Bezpředložkové vazby
|                    |                               |
| ---                | ---                           |
| anrufen 4.p.       | (za)telefonovat někomu        |
| sich ansehen 4.p.  | podívat se na, prohlédnout si |
| glauben 4.p        | věřit čemu                    |
| heiraten 4.p.      | ženit se s, vdávat se za      |
| holen 4.p.         | dojít pro                     |
| kennen lernen 4.p. | seznámit se s, poznat         |
| vergessen 4.p.     | zapomenout na                 |
| verstehen 4.p.     | rozumět komu, čemu            |

## Příklady

Ich denke darüber **nach**.
**Worum** bittest du ihn?
Sie wartet lange **darauf**.
Er entschuldigt sich **dafür**.
Wir fragen **danach**.

Ich bitte **darum**.
**Worüber** schreibst du?
Sie arbeitet lange **daran**?
Er insteressiert sich **dafür**.
Wir hoffen **darauf**.