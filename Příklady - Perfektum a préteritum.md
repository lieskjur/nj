Perfektum a préteritum
======================

ich empfehle      | empfahl             | habe empfohlen          | doporučit
ihr lauft         | lieft               | seid gelaufen           | běžet
er sendet         | er sendete          | hat gesendet            | poslat
er bietet an      | er bot an           | hat angeboten           | nabídnout
sie will arbeiten | sie wollte arbeiten | sie hat arbeiten wollen | chtít pracovat

Niemand kannte ihn                   | Niemand hat ihn gekannt                    |
Er zog in eine neue Wohnung um       | Er ist in eine neue Wohnung umgezogen      |
Sie verzieh es uns                   | Sie hat es uns verziehen                   |
Wir unternammen einen langen Ausflug | Wir haben einen langen Ausflug unternommen |
Wo verbrachtest du das Wochenende    | Wo hast du das Wochenende verbracht        |
Kannte sie jemand?                   | Hat sie jemand gekannt?                    |
Wir luden unsere Freunde ein         | Wir haben unsere Freunde eingeladen        |

ich esse | ich aß | ich habe gegessen
ihr seht | ihr saht | ihr hat gesehen
er kennt | er kannte | er hat gekannt
er bittet | er bat | er hat gebeten
sie will arbeiten | sie wollte arbeiten | sie arbeiten wollen

er will | er wollte | er hat gewollt
du schläfst | du schliefst | du hast geschlafen
wir empfehlen | wir empfahlen | wir haben empfohlen
du wäschst | du wuschst | du hast gewaschen

ich lese | ich las | ich habe gelesen
ihr kommt | ihr kamt | ihr seid gekommen
du lernst | du lernt | du hast gelernt
es brennt | es brannte | es hat gebrannt
sie muss lesen | sie musste lesen | sie lesen müssen