Zvratná slovesa
===============

| se   | si   |
| --   | --   |
| mich | mir  |
| dich | dir  |
| sich | sich |
| uns  | uns  |
| euch | euch |
| sich | sich |

## Příklady

1. Er wäscht __sich__ die Hände.
2. Warum kaufst du es __dir__ nicht?
3. Kinder, warum setzt ihr __euch__ nicht?
4. Ich muss __mich__ entschuldigen.
5. Sie sollen __sich__ zuerst vorstellen.
---
1. Sie wäscht __sich__ die Hände.
2. Warum stellst du __dich__ nicht vor?
3. Kinder, warum entschuldigt ihr __euch__ nicht?
4. Ich muss __mir__ die Zähne putzen.
---
1. Ich wasche __mir__ die Hände.
2. Warum kaufst du es __dir__ nicht?
3. Kinder, warum setzt ihr __euch__ nicht?
4. Wir müssen __uns__ entschuldigen.
5. Sie sollen __sich__ zuerst vorstellen.

## Nevyplňené příklady

1. Er wäscht ____ die Hände.
2. Warum kaufst du es ____ nicht?
3. Kinder, warum setzt ihr ____ nicht?
4. Ich muss ____ entschuldigen.
5. Sie sollen ____ zuerst vorstellen.
---
1. Sie wäscht ____ die Hände.
2. Warum stellst du ____ nicht vor?
3. Kinder, warum entschuldigt ihr ____ nicht?
4. Ich muss ____ die Zähne putzen.
---
1. Ich wasche ____ die Hände.
2. Warum kaufst du es ____ nicht?
3. Kinder, warum setzt ihr ____ nicht?
4. Wir müssen ____ entschuldigen.
5. Sie sollen ____ zuerst vorstellen.