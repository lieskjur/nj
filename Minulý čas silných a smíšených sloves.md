Minulý čas silných a smíšených sloves
=====================================

## Silná slovesa

### Préteritum silných sloves
| ich         | du        | er          | wir       | ihr      | sie/Sie   |
| ---         | --        | --          | ---       | ---      | -------   |
| **kam**     | kamst     | **kam**     | kamen     | kamt     | kamen     |
| **schrieb** | schreibst | **schrieb** | schrieben | schriebt | schrieben |

### Skupina A-B-A
|           |        |           |         |            |
| ---       | ---    | ---       | ---     | ---        |
| fangen    | chytat | fängst    | fing    | gefangen   |
| an/fangen | začít  | fängst an | fing an | angefangen |
| geben     | dát    | gibst     | gab     | gegeben    |
| schlagen  | bít    | schlägst  | slug    | geschlagen |
| tragen    | nosit  | trägst    | trug    | getragen   |
| waschen   | mýt    | wäschst   | wusch   | gewaschen  |
| lesen     | číst   | liest     | las     | gelesen    |
|           |        |           |         |            |
| ein/laden | pozvat | ?         | lud ein | eingeladen |

### Skupina A-B-B
|             |            |           |                  |
| ---         | ---        | ---       | ---              |
| bieten      | poskytovat | bot       | geboten          |
| bleiben     | zůstat     | blieb     | bin geblieben    |
| frieren     | mrznout    | fror      | gefroren         |
| genießen    | užívat     | genoss    | genossen         |
| leihen      | pujčit     | lieh      | geliehen         |
| entscheiden | rozhodnout | entschied | entschieden      |
| scheinen    | svítit     | schien    | geschienen       |
| schreiben   | psát       | schrieb   | geschrieben      |
| stehen      | stát       | stand     | gestanden        |
| auf/stehen  | vstát      | stand auf | bin aufgestanden |
| verstehen   | rozumět    | verstand  | verstanden       |
| steigen     | stoupat    | stieg     | ist gestiegen    |
| treiben     | provozovat | trieb     | getrieben        |
| tun         | dělat      | tan       | getan            |
| verzeihen   | prominout  | verzieh   | verziehen        |
| ziehen      | táhnout    | zog       | gezogen          |
|             |            |           |                  |

---
ie → o
ei → ie
e/u → a

### Skupina A-B-C
|            |           |           |         |            |
| ---        | ---       | ---       | ---     | ---        |
| beginnen   | začít     |           | begann  | begonnen   |
| binden     | vázat     |           | band    | gebunden   |
| ver/binden | spojit    |           | verband | verbunden  |
| bitten     | prosit    |           | bat     | gebeten    |
| brechen    | zlomit    |           | brach   | gebrochen  |
| empfehlen  | doporučit | empfielst | empfahl | empfohlen  |
| finden     | najít     |           | fand    | gefunden   |
| gehen      | jít       |           | ging    | gegangen   |
| helfen     | pomoct    | hilfst    | half    | geholfen   |
| sprechen   | mluvit    | sprichst  | sprach  | gesprochen |

## Smíšená slovesa
|               |                |                |                  |
| ---           | ---            | ---            | ---              |
| brennen       | hořet          | brannte        | gebrannt         |
| bringen       | přinést        | brachte        | gebracht         |
| verbringen    | strávit        | verbrachte     | verbracht        |
| denken        | myslet         | dachte         | gedacht          |
| nach/denken   | myslet na      | dachte nach    | nachgedacht      |
| kennen        | znát           | kannte         | gekannt          |
| nennen        | nazývat        | nannte         | gennant          |
| rennen        | utíkat/pád     | rannte         | gerannt          |
| senden        | poslat/vysílat | sandte/sendete | gesandt/gesendet |
| (sich) wenden | obrátit (se)   | wandte/wandete | gewandt/gewendet |