Stupňování přídavných jmen
==========================

## v přísudku
- tak starý jako - so alt wie
- starší než - ält**er** als
- nejstarší z - die ält**este** von
- Er ist am ält**esten** - Je nejstarší

## v přívlastku
- Das ist **der** älter**e** Bruder von Eva - To je starší Bratr Evy
- Ich habe ein**en** jünger**en** Bruder - Mám mladšího bratra
- Ich kenne **kein** bessere**s** Beispiel - Neznám žádný lepší příklad
- Das ist **unser** best**er** Student - Je to náš nejlepší student

## Nepravidelné tvary
| groß		| gr*ö*ßer	| der gr*ö*ßte	|
| gut 		| besser	| der besste	|
| nah		| näher		| der nä*ch*ste	|
| hoch 		| h*öh*er	| der h*ö*chste	|
| dunkel	| dun*kl*er	| der dunkelste	|

## Příklady
das kleinste Kind
Er studiert besser
die Wohnung ist die älteste
mein kleinerer Bruder
Sie kommen am spätesten

die neueren Kinder
Er studiert besser
die Wohnung ist älter
mein kleinster Bruder
sie kommen früher

das ältere Kind
Er studiert am besten
die Wohnung ist neuer
mein kleinster Bruder
sie kommen später

Der älteste Mann - Nejstarší muž