Rozkazovací způsob
==================

- odlučitelná předpona je za slovesem
- v 2. osobě
	- nemají osobní zájmeno: Bitte ihn - popros ho
	- mají povinnou koncovku -e po -t,-d,-ig,-m,-n,-eln,-ern
	- u silných sloves: -e- → -i- (nimm! - ber!, gib! - dej!, lies! - čti)
- v 1. a 3. osobě množného čísla
	- zájmeno stojí za slovesem

## Soveso *sein*
má v rozkazovacím způsobu nepravidelné tvary
* Sei so gut! - buď také hodný!
* Seien Sie bitte so nett! - Buďte prosím tak laskav!
* Seien wir nett zu ihr! - Buďme k ní milí!
* Seid auch nett zu ihr! - Buďte k ní také milí!

## Příklady
|		| jedn. č	| množ. č.		|
|-------|-----------|---------------|
| 1. os	|			| Warten wir!	|
| 2. os | Warte!	| Wartet!		|
| 3. os | 			| Warten Sie!	|

* Bitte ihn! - Popros ho!
* Macht es doch! - Udělejte to přeci!
* Kommen Sie bitte auch! - Přijďte prosím také!

- du, fahren → Fahr!
- ihr, vorbereiten → Bereitet euch vor!
- wir, schlafen gehen → Gehen wir schlafen!
- du, verkaufen → Verkauf!
- Sie, sein → Seien sie!

- du, schreiben → Schreib!
- ihr, einkaufen → Kauft ein!
- Sie, arbeiten gehen → Gehen Sie arbeiten!
- du, lessen → Lies!
- wir, sein → Seien wir!

- ihr, schlaft → Schlaft!
- du, vorbereiten → Bereite dich vor!
- Sie, lernen gehen → Gehen Sie lernen!
- du, einkaufen → Kauf ein!
- ihr, sein → Seid!

### L3/19
* du - deine Freundin begleiten → Begleite deine Freundin!, 
* ihr - Frau Neuss grüßen → Grüßt Frau Neuss!, 
* wir - es dem Vater sagen → Sagen wir es dem Vater!, 
* Sie - jetzt Deutsch sprechen → Sprechen Sie jetzt Deutsch!,
* ihr - jetzt lernen -> Lernt jetzt!
* du - es mir sagen -> Sag es mir!
* Sie - meinen Vater jetzt nicht stören -> Stören Sie meinen Vater jetzt nicht!
* wir - zuerst nach links gehen -> Gehen wir zuerst nach links
* du - sofort kommen -> Komm sofort!